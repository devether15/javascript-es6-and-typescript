//normal function [es5]
// var greet = function(msg){
//     return msg;
// }

// console.log(greet('hello'))

//es6 fat arrow with one parameter

// let greet = msg => msg
// console.log(greet('hello'))

//arrow function with several parameters

// let greet = (msg, name) => msg+' '+name
// console.log(greet('hello','Joseph'))

//arrow function without parameters

// let greet = () => 'hello Joseph'
// console.log(greet())

//calling the function from another function
// setTimeout(() => console.log('Hello from ES6'),1000);

//if the function have several sentences

greet = () => {
    msg = 'hello'
    name = 'Joseph'
    console.log(msg + ' ' + name)
}

greet()