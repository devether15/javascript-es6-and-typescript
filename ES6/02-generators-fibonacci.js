let fibonacci = {
    *[Symbol.iterator]() {
        let previous = 0, current = 1
        for (;;){
            [ previous, current ] = [ current, previous + current]
            yield current
        }
    } 
}

for(let n of fibonacci) {
    if (n > 1000) break
    console.log(n)
}