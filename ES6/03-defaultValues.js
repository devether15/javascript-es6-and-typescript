//ES5
function multiplication (a,b) {
    b = typeof b !== 'undefined' ? b : 1;
    return a*b;
}

console.log(multiplication(5)); //5


//ES6
function multiplicar(a,b = 2){
    return a*b
}

console.log(multiplicar(5)) //5  


//primitive types
function f (x,y = 7,z = 42) {
    return x + y + z
}

console.log(f(1)) //50


//arrays as default values in arguments/
function agregar(valor, arreglo = []) {
    arreglo.push(valor)
    return arreglo
}

console.log((1)); //[1]
console.log((2)); //[2], no [1, 2]


//functions as default values in arguments/
function callingSomething(stuff = somethingElse()) { return stuff }

function somethingElse(){
    return Math.floor(Math.random()*10);
}

console.log(callingSomething()); //ramdom number