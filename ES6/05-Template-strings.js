var string1 = `Esto es una cadena`;
console.log(string1);

var number = 10;
var name = 'Joseph';
var string2 = `My name is ${name} and I have ${number} working at the company`;
console.log(string2);

//we can add spaces like this:
var number = 8;
var name = 'Adrian';
var string2 = `My name is 
${name},
 and I have
  ${number}
   working at the company`;
console.log(string2);



