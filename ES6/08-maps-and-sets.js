//sets
let set = new Set()
//add 3 elements, strign1 is repeated
set.add('string1').add('string2').add('string1')
//the size is 2
console.log(set.size == 2)//true
//the set doesn't have de string "hello"
console.log(set.has('Hello'))//false


//maps
let map = new Map()
//add the key "hello" with the value 42
map.set('hello', 42)
//add the key "bye" with the value 34
map.set('bye', 34)
//get the value associated with the key "bye"
console.log(map.get('hello')) //42
console.log(map.get('bye')) //34