//destructuring is a method to extract data quickly forn an object {} or an array [] 
//without writing to much code.

//example 1, destructuring and array
let foo =[1,2,3]
let [one,two,three] = foo
console.log(one) // outputs 1
console.log(two) // outputs 2
console.log(three) // outputs 3

//example 2, destructuring an object
let module = {
    square(lon) { console.log(lon*lon); };
    circle(radius) { console.log(radius*Math.PI); };
    Text(text) { console.log(text); };
};
let {square,text,circle} = module;
square(5)
text('Hello')
circle(10)
//continuar 3:02
