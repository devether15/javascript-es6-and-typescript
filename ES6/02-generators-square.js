function* square() {
    let number = 1
    while(true) {
        let getSquare = number * number
        number++
        yield getSquare
    }
}

let generator = square()
console.log(generator) // "suspended"
console.log(generator.next().value) //1
console.log(generator.next().value) //4
console.log(generator.next().value) //9
console.log(generator.next().value) //16

