function* range (start, end, increase) {
    while (start < end) {
        yield start
        start+= increase
    }
}

for (let i of range(0,10,2)) {
    console.log(i)
}