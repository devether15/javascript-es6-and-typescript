//1

var sim1 = Symbol();
var sim2 = Symbol('foo');
var sim3 = Symbol('foo');

console.log(sim1);
console.log(sim2);
console.log(sim3);
//the param of Symbol, is just a description, not a variable
console.log(sim2 === sim3); //false

//2

console.log(typeof sim2);//'symbol'

var symObj = Object(sim2);
console.log(typeof symObj);//'object'

//3

//you can't make an instance with new...
//var sym = new Symbol();//type error

//4

//better to use them as const, because they never change,
//but thwe still will be type symbol:

const foo = Symbol('Este simbolo es foo')
const bar = Symbol('Este simbolo es bar')
console.log(typeof foo==='symbol')//true
console.log(typeof bar==='symbol')//true

//5

let obj = {}
obj[foo] = 'foo'
obj[bar] = 'bar'
console.log(JSON.stringify(obj)) //{}
console.log(Object.keys(obj)) //[]
console.log(Object.getOwnPropertyNames(obj)) //[]
console.log(Object.getOwnPropertySymbols(obj)) //[ foo, bar ]


